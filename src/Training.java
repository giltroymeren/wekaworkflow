import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.output.prediction.AbstractOutput;
import weka.classifiers.evaluation.output.prediction.CSV;
import weka.classifiers.meta.LogitBoost;
import weka.classifiers.meta.RandomSubSpace;
import weka.classifiers.rules.ZeroR;
import weka.classifiers.trees.ADTree;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.Range;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/**
 * Parts adapted from
 * http://weka.wikispaces.com/file/view/CrossValidationMultipleRuns
 * .java/82916745/CrossValidationMultipleRuns.java
 * 
 * @author Troy
 * 
 */
public class Training {

	protected final static Log logger = LogFactory.getLog(Training.class);
	private static String PATH_DATA = "C:/Users/Troy/Dropbox/sp-2013/workshop/data/arff/";
	private static String PATH_RESULTS = "C:/Users/Troy/Dropbox/sp-2013/workshop/results/";
	private static String DATASET_NAME = "bosom.150k";
	private static String DATASET_TIME = "10";
	private static final String HEADER_CSV = "Iteration #,Correctly Classified Instances,"
			+ "Incorrectly Classified Instances,Correctly Classified Instances (%),"
			+ "Incorrectly Classified Instances (%),Kappa statistic,Mean absolute error,"
			+ "Root mean squared error,Relative absolute error,Root relative squared error,"
			+ "Coverage of cases (0.95 level),Mean rel. region size (0.95 level),"
			+ "TP Rate (0),FP Rate (0),Precision (0),Recall/Sensitivity (0),Specificity (0),"
			+ "F-Measure (0),MCC (0),ROC Area (0),PRC Area (0),TP Rate (1),FP Rate (1),"
			+ "Precision (1),Recall/Sensitivity (1),Specificity (1),F-Measure (1),"
			+ "MCC (1),ROC Area (1),PRC Area (1),"
			+ "TP (0),TN (0),FP (0),FN (0),TP (1),TN (1),FP (1),FN (1)\n";

	/**
	 * 
	 * @param instance
	 * @return
	 * @throws Exception
	 */
	private static Instances removeAttributes(Instances instance)
			throws Exception {
		// http://weka.wikispaces.com/Use+Weka+in+your+Java+code#Filter
		// String[] optionsRemove = new String[] { "-R",
		// "3,4,5,6,7,8,10,11,13,14,15,16,18,19,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35"
		// };
		String[] optionsRemove = new String[] { "-R", "31,32,33,34,35" };
		Remove remove = new Remove();
		remove.setOptions(optionsRemove);
		remove.setInputFormat(instance);

		Instances instanceFiltered = Filter.useFilter(instance, remove);
		return instanceFiltered;
	}

	/**
	 * 
	 * @param fileName
	 * @param path
	 * @return
	 * @throws Exception
	 */
	private static Instances getData(String fileName) throws Exception {
		BufferedReader bufferedReader = null;
		bufferedReader = new BufferedReader(new FileReader(PATH_DATA + fileName
				+ ".ARFF"));
		Instances train = new Instances(bufferedReader);
		bufferedReader.close();
		return train;
	}

	/**
	 * 
	 * @return
	 */
	private static AbstractOutput getAbstractObject() {
		StringBuffer predictionSB = new StringBuffer();
		CSV output = new CSV();
		output.setBuffer(predictionSB);
		output.setOutputDistribution(true);
		return output;
	}

	private static String getAssembledCvCsvResults(int foldNum,
			Evaluation evaluation) throws Exception {
		StringBuffer resultSB = new StringBuffer();

		resultSB.append(foldNum + ",");
		resultSB.append(evaluation.correct() + ",");
		resultSB.append(evaluation.incorrect() + ",");
		resultSB.append(evaluation.pctCorrect() + ",");
		resultSB.append(evaluation.pctIncorrect() + ",");
		resultSB.append(evaluation.kappa() + ",");
		resultSB.append(evaluation.meanAbsoluteError() + ",");
		resultSB.append(evaluation.rootMeanSquaredError() + ",");
		resultSB.append(evaluation.relativeAbsoluteError() + ",");
		resultSB.append(evaluation.rootRelativeSquaredError() + ",");
		resultSB.append(evaluation.coverageOfTestCasesByPredictedRegions()
				+ ",");
		resultSB.append(evaluation.sizeOfPredictedRegions() + ",");

		resultSB.append(evaluation.truePositiveRate(0) + ",");
		resultSB.append(evaluation.falsePositiveRate(0) + ",");
		resultSB.append(evaluation.precision(0) + ",");
		resultSB.append(evaluation.recall(0) + ",");
		resultSB.append(evaluation.numTrueNegatives(0)
				/ (evaluation.numTrueNegatives(0) + evaluation
						.numFalsePositives(0)) + ",");
		resultSB.append(evaluation.fMeasure(0) + ",");
		resultSB.append(evaluation.matthewsCorrelationCoefficient(0) + ",");
		resultSB.append(evaluation.areaUnderROC(0) + ",");
		resultSB.append(evaluation.areaUnderPRC(0) + ",");
		resultSB.append(evaluation.truePositiveRate(1) + ",");
		resultSB.append(evaluation.falsePositiveRate(1) + ",");
		resultSB.append(evaluation.precision(1) + ",");
		resultSB.append(evaluation.recall(1) + ",");
		resultSB.append(evaluation.numTrueNegatives(1)
				/ (evaluation.numTrueNegatives(1) + evaluation
						.numFalsePositives(1)) + ",");
		resultSB.append(evaluation.fMeasure(1) + ",");
		resultSB.append(evaluation.matthewsCorrelationCoefficient(1) + ",");
		resultSB.append(evaluation.areaUnderROC(1) + ",");
		resultSB.append(evaluation.areaUnderPRC(1) + ",");

		resultSB.append(evaluation.numTruePositives(0) + ",");
		resultSB.append(evaluation.numTrueNegatives(0) + ",");
		resultSB.append(evaluation.numFalsePositives(0) + ",");
		resultSB.append(evaluation.numFalseNegatives(0) + ",");

		resultSB.append(evaluation.numTruePositives(1) + ",");
		resultSB.append(evaluation.numTrueNegatives(1) + ",");
		resultSB.append(evaluation.numFalsePositives(1) + ",");
		resultSB.append(evaluation.numFalseNegatives(1) + "\n");

		return resultSB.toString();
	}

	/**
	 * 
	 * @param classifier
	 * @param instance
	 * @param numFolds
	 * @param output
	 * @return
	 * @throws Exception
	 */
	private static Evaluation getEvaluation(Classifier classifier,
			Instances instance, AbstractOutput output, String fileName)
			throws Exception {
		int numCvIters = 10;
		int numFolds = 10;
		Evaluation evaluation = null;
		Range attributesToShow = null;
		Boolean outputDistributions = new Boolean(true);

		String preds = "";
		StringBuffer cvIterResultSB = new StringBuffer();
		cvIterResultSB.append(HEADER_CSV);

		// 10 x 10 CV
		for (int i = 1; i <= numCvIters; i++) {
			int seed = i + 15;
			Random rand = new Random(seed);
			Instances randomData = new Instances(instance);
			randomData.randomize(rand);

			// do CV
			evaluation = new Evaluation(randomData);
			evaluation.crossValidateModel(classifier, instance, numFolds, rand,
					output, attributesToShow, outputDistributions);

			cvIterResultSB.append(getAssembledCvCsvResults(i, evaluation));

			// String results = "=== " + numFolds
			// + "-fold cross-validation run # " + i + " ===\n\n"
			// + "Classifier:\t" + classifier.getClass().getName() + "\n"
			// + "Folds:\t\t" + numFolds + "\n" + "Seed:\t\t" + seed
			// + "\n\n" + evaluation.toSummaryString(false) + "\n"
			// + evaluation.toClassDetailsString() + "\n"
			// + evaluation.toMatrixString() + "\n";

			// preds += "=== Predictions for run # " + i + " ===\n"
			// + output.getBuffer().toString() + "\n\n";
		}

		saveResultBuffer(cvIterResultSB.toString(), "/cv-results/" + fileName
				+ ".folds.results.CSV");
		// saveResultBuffer(preds, "/cv-preds/" + fileName +
		// "-folds-preds.CSV");

		return evaluation;
	}

	/**
	 * 
	 * @param classifier
	 * @param instance
	 * @return
	 */
	private static String getRunInformation(Classifier classifier,
			Instances instance) {
		String resultString = "";
		StringBuilder resultSB = new StringBuilder();

		resultString += "Scheme:\t\t" + classifier.getClass() + "\n"
				+ "Relation:\t" + instance.relationName() + "\n"
				+ "Instances:\t" + instance.numInstances() + "\n"
				+ "Attributes:\t" + instance.numAttributes() + "\n";

		for (int i = 0; i < instance.numAttributes(); i++) {
			resultSB.append("\t\t\t" + instance.attribute(i).name() + "\n");
		}

		resultString += resultSB.toString() + "\n"
				+ "Test mode:\t10-fold cross-validation" + "\n\n";
		return resultString;
	}

	/**
	 * 
	 * @param classifier
	 * @return
	 */
	private static String getClassifierModelResult(Classifier classifier) {
		return classifier.toString() + "\n" + "Time taken to build model:\t"
				+ "\n\n";
	}

	/**
	 * 
	 * @param evaluation
	 * @return
	 * @throws Exception
	 */
	private static String getEvaluationResults(Evaluation evaluation)
			throws Exception {
		return evaluation.toSummaryString(true) + "\n"
				+ evaluation.toClassDetailsString() + "\n"
				+ evaluation.toMatrixString() + "\n";
	}

	/**
	 * 
	 * @param results
	 * @param fileName
	 */
	private static void saveResultBuffer(String results, String fileName) {
		Writer writer = null;
		String fullFilePathAndName = PATH_RESULTS + fileName;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(fullFilePathAndName), "UTF-8"));
			writer.write(results);
			results = null;
			logger.info("Successfully created file [" + fullFilePathAndName
					+ "]\n");
		} catch (IOException e) {

		} finally {
			try {
				writer.close();
			} catch (Exception e) {

			}
		}
	}

	/**
	 * http://weka.wikispaces.com/Serialization
	 * 
	 * @param classifier
	 * @param fileName
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private static void saveTrainedModel(Classifier classifier, String fileName)
			throws FileNotFoundException, IOException {
		String fullFilePathAndName = PATH_RESULTS + "/models/" + fileName
				+ ".MODEL";
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(
				fullFilePathAndName));
		logger.info("Successfully created model [" + fullFilePathAndName
				+ "]\n");
		oos.writeObject(classifier);
		classifier = null;
		oos.flush();
		oos.close();
	}

	/**
	 * 
	 * @param classifier
	 * @param instance
	 * @param fileName
	 * @throws Exception
	 */
	private static void assemble(Classifier classifier, Instances instance,
			String fileName) throws Exception {
		CSV predictionOutput = (CSV) getAbstractObject();
		predictionOutput.setNumDecimals(6);

		Evaluation evaluation = getEvaluation(classifier, instance,
				predictionOutput, fileName);

		StringBuffer resultBuffer = new StringBuffer();
		resultBuffer.append("=== Run information ===\n\n");
		resultBuffer.append(getRunInformation(classifier, instance));
		resultBuffer.append("=== Classifier model (full training set) ===\n\n");
		resultBuffer.append(getClassifierModelResult(classifier));
		resultBuffer
				.append("=== Predictions on test data ===\n\nsee associated CSV file \n");
		resultBuffer.append(getEvaluationResults(evaluation));

		saveResultBuffer(resultBuffer.toString(), "/main-results/" + fileName
				+ ".result.buffer.TXT");
		saveResultBuffer(predictionOutput.getBuffer().toString(),
				"/main-preds/" + fileName + "-preds.CSV");
		saveTrainedModel(classifier, fileName);
	}

	public static void main(String[] args) throws Exception {

		Instances train = removeAttributes(getData(DATASET_NAME));
		train.setClassIndex(train.numAttributes() - 1);

//		ZeroR zr = new ZeroR();
//		zr.buildClassifier(train);
//		assemble(zr, train, DATASET_NAME + "." + DATASET_TIME + "." + "zr");

		RandomForest rf = new RandomForest();
		rf.buildClassifier(train);
		assemble(rf, train, DATASET_NAME + "." + DATASET_TIME + "." + "rf");

		LogitBoost lb = new LogitBoost();
		lb.buildClassifier(train);
		assemble(lb, train, DATASET_NAME + "." + DATASET_TIME + "." + "lb");

		RandomSubSpace rs = new RandomSubSpace();
		rs.buildClassifier(train);
		assemble(rs, train, DATASET_NAME + "." + DATASET_TIME + "." + "rs");

		J48 j48 = new J48();
		j48.buildClassifier(train);
		assemble(j48, train, DATASET_NAME + "." + DATASET_TIME + "." + "j48");

		ADTree adt = new ADTree();
		adt.buildClassifier(train);
		assemble(adt, train, DATASET_NAME + "." + DATASET_TIME + "." + "adt");
	}
}
